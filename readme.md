# Container for slideint

### Command to build the Docker image

The container is described in file [dockerfile](./dockerfile).

In order to build the image of the container, run the following command.

```
docker build --rm --build-arg UID=$(id -u) --build-arg GID=$(id -g) --tag slideint:latest -f dockerfile Context
```

The user that is added in the container, which is named `slideint` has the user's `uid` and `gid`.

### Command to run the Docker container

The container is started with user's `uid` and `gid` and uses Maven local repository. You should execute the following command in the directory that contains the slideint project.

```
docker run -it --rm --user "$(id -u)":"$(id -g)" -v $(pwd):/home/slideint/project -v $HOME/.m2:/home/slideint/.m2 slideint
```

