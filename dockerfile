FROM debian:testing-slim

CMD ["/bin/bash"]

# set default values for build arguments
ARG UNAME=slideint
ARG UID=1000
ARG GID=1000

# install packages and umlet software
# openjdk-17-jdk  : required by slideint dependencies
# maven           : for code examples using command mvn
# wget unzip      : to install umlet
# locales         : to add locales
# make            : to execute slideint commands
# graphviz        : for plantuml
# transfig        : for conversion from Xfig
# dia             : for dia
# imagemagick     : for converting images
# inkscape        : for conversion of svg files
# cairosvg        : for conversion of svg files
# mktexlsr        : update tex ls-R databases (due to install by dpkg -i)
RUN /bin/sh -c set -eux; \
    apt-get update -y; \
    apt-get install -y --no-install-recommends \
            openjdk-17-jdk \
            maven \
            wget \
            unzip \
            locales \
            make \
            graphviz \
            transfig \
            dia \
            imagemagick \
            inkscape \
            cairosvg \
            dvipdfmx \
            dvipng \
            lmodern \
            recode \
            tex4ht \
            texlive \
            texlive-full \
            texlive-lang-all \
            tidy \
            git \
            emacs \
            openssh-client \
            rsync; \
    sed -i -e 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# en_IE.UTF-8 UTF-8/en_IE.UTF-8 UTF-8/' /etc/locale.gen && \
    sed -i -e 's/# fr_FR@euro ISO-8859-15/fr_FR@euro ISO-8859-15/' /etc/locale.gen && \
    sed -i -e 's/# en_GB.ISO-8859-15 ISO-8859-15/en_GB.ISO-8859-15 ISO-8859-15/' /etc/locale.gen && \
    sed -i -e 's/# en_IE@euro ISO-8859-15/en_IE@euro ISO-8859-15/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales; \
    apt-get update -y; \
    mkdir -p /opt; \
    cd /opt; \
    wget --progress dot:giga -O umlet.zip https://www.umlet.com/download/umlet_14_3/umlet-standalone-14.3.0.zip; \
    unzip -qq umlet.zip; \
    rm -f umlet.zip; \
    ln -s /opt/Umlet/umlet.sh /opt/Umlet/umlet; \
    chmod a+x /opt/Umlet/umlet.sh

# change default configuration for command convert
RUN echo '<policy domain="coder" rights="read | write" pattern="PDF" />' >> /etc/ImageMagick-6/policy.xml; \
    echo '<policy domain="coder" rights="read | write" pattern="EPS" />' >> /etc/ImageMagick-6/policy.xml

# create user and configure .bashrc
RUN groupadd -g $GID -o $UNAME; \
    useradd -m -u $UID -g $GID -o -s /bin/bash $UNAME; \
    mkdir /home/$UNAME/project; \
    echo "export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64" >> /home/$UNAME/.bashrc; \
    echo "export UMLET_HOME=/opt/Umlet/umlet.sh" >> /home/$UNAME/.bashrc; \
    echo "PATH=\${PATH}:/opt/Umlet" >> /home/$UNAME/.bashrc; \
    mkdir -p /home/$UNAME/.m2/repository/

# beamerthemes and slideint debian files
ARG BEAMERFILE=beamerthemes_20200130_all.deb
ARG SLIDEINT=slideint_20220428_all.deb

# copy deb files
COPY $BEAMERFILE /root/$BEAMERFILE
COPY $SLIDEINT /root/$SLIDEINT

# install beamerthemes and slideint
RUN /bin/sh -c set -eux; \
    apt-get update -y; \
    dpkg -i /root/$BEAMERFILE; \
    apt-get update -y; \
    dpkg -i /root/$SLIDEINT; \
    apt-get update -y; \
    mktexlsr; \
    rm -rf /var/lib/apt/lists/*; \
    rm -f /root/$BEAMERFILE /root/$SLIDEINT;

# set user, environment, and working directory
USER $UNAME
ENV HOME=/home/$UNAME \
    LANG=fr_FR.UTF-8 \
    LANGUAGE=fr_FR.UTF-8 \
    LC_ALL=fr_FR.UTF-8 \
    PS1=$
WORKDIR /home/$UNAME/project

CMD /bin/bash

